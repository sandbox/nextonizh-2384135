<?php

/**
 * @file
 * Definition of views_handler_daterange_filter.
 */

/**
 *  Filter to handle dates stored as a timestamp.
 */
class views_handler_daterange_filter extends views_handler_filter {

  function option_definition() {
    $options = parent::option_definition();

    // value is already set up properly, we're just adding our new field to it.
    $options['value']['contains']['type']['default'] = 'date';
    $options['operator']['default'] = 'between_inline';
    $options['value']['contains'] += array(
      'alignment' => array('default' => 'right'),
      'months' => array('default' => 2),
    );
    return $options;
  }

  function operators() {
    $operators = array(
      'between_inline' => array(
        'title' => t('Is between (Range datepicker)'),
        'method' => 'op_between_inline',
        'short' => t('between (Range datepicker)'),
        'values' => 1
      )
    );

    return $operators;
  }

  /**
   * Add a type selector to the value form
   */
  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);

    // Run time
    if (!empty($form_state['exposed'])) {

      $form['value']['#tree'] = TRUE;

      // When exposed we drop the value-value and just do value if
      // the operator is locked.
      $form['value'] = array(
        '#type' => 'textfield',
        '#title' => empty($form_state['exposed']) ? t('Value') : '',
        '#size' => 30,
        '#default_value' => $this->value['value'],
      );


      $form['value']['months'] = array(
        '#type' => 'value',
        '#value' => !empty($this->value['months']) ? $this->value['months'] : 0,
      );
      $form['value']['alignment'] = array(
        '#type' => 'value',
        '#value' => !empty($this->value['alignment']) ? $this->value['alignment'] : 'right',
      );

      if (empty($form['#after_build'])) {
        $form['#after_build'] = array();
      }
      if (!in_array('views_daterange_filter_exposed_form_after_build', $form['#after_build'])) {
        $form['#after_build'][] = 'views_daterange_filter_exposed_form_after_build';
      }
    } else {
      $form['value']['months'] = array(
        '#type' => 'select',
        '#title' => 'Number of months',
        '#options' => drupal_map_assoc(array(1, 2, 3, 4)),
        '#default_value' => !empty($this->value['months']) ? $this->value['months'] : 0,
      );

      $form['value']['alignment'] = array(
        '#type' => 'radios',
        '#title' => 'Calendar Alignment',
        '#options' => array(
          'left' => t('Left'),
          'right' => t('Right'),
        ),
        '#default_value' => !empty($this->value['alignment']) ? $this->value['alignment'] : 'right',
      );
    }
  }

  function op_between_inline($field) {

    //Exit if empty value
    if (empty($this->value[0]))
      return;

    $value = explode(' - ', $this->value[0]);
    $a = intval(strtotime($value[0]));
    $b = intval(strtotime($value[1]));

    if (date('H', $b) == '00' && date('i', $b) == '00' && date('s', $b) == '00') {
      $b = strtotime(date('d.m.Y', $b) . '23:59:59');
    }

    if (in_array($this->definition['field_type'], array('datetime','date'))){
      $field = 'UNIX_TIMESTAMP('.$field.')';
    }

    // This is safe because we are manually scrubbing the values.
    // It is necessary to do it this way because $a and $b are formulas when using an offset.
    $operator = strtoupper('BETWEEN');
    $this->query->add_where_expression($this->options['group'], "$field $operator $a AND $b");
  }

  function admin_summary() {
    if (!empty($this->options['exposed'])) {
      return t('exposed');
    }
    return '';
  }

  function query() {
    $this->ensure_my_table();
    $field = "$this->table_alias.$this->real_field";
    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}($field);
    }
  }

}