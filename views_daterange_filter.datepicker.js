(function ($, Drupal) {

  Drupal.behaviors.DateRangeFilterDatepicker = {
    attach: function (context, settings) {
      if (!$.fn.datepicker || !Drupal.settings.DateRangeFilter) {
        return;
      }

      $.datepicker._defaults.onAfterUpdate = null;

      var datepicker__updateDatepicker = $.datepicker._updateDatepicker;
      $.datepicker._updateDatepicker = function( inst ) {
        datepicker__updateDatepicker.call( this, inst );

        var onAfterUpdate = this._get(inst, 'onAfterUpdate');
        if (onAfterUpdate) {
          onAfterUpdate.apply((inst.input ? inst.input[0] : null),
            [(inst.input ? inst.input.val() : ''), inst]);
        }
      }


      if (Drupal.settings.DateRangeFilter){
        for(var form_id in Drupal.settings.DateRangeFilter)
          var form = Drupal.settings.DateRangeFilter[form_id];
        for(var filter_id in form){
          if ($('#'+filter_id).length >0)
            applyDateRangeFilter('#'+form_id, form[filter_id]);

        }
      }
    }
  }

  function applyDateRangeFilter(form_id, filter){
    var localContext = $(form_id);
    var filterEl     = $(filter.id, localContext);
    var calendarIcon = '<button type="button" class="drf-datepicker-trigger tooltip-processed"> </button>';

    if (!filterEl.parent().find("button.drf-datepicker-trigger").length) {
      if (filter.calendarAlignment == 'left')
        filterEl.parent().prepend(calendarIcon);
      else
        filterEl.parent().append(calendarIcon);
    }

    $("button.drf-datepicker-trigger").live('click', function(){
      filterEl.focus();
    });

    $("button.ui-datepicker-select").live('click', function(){
      var currentFormId = $(this).attr('data-form-id');
      var filterEl = $($(this).attr('data-filter-id'));

      if (currentFormId) {
        if (form_id == currentFormId) {
          var start = filterEl.attr('data-start');
          var end   = filterEl.attr('data-end');

          if (start !== "" && end !== "") {
            filterEl.val(start + " - " + end);
          }
          else if (start !== "" && end === "") {
            filterEl.val(start);
          }

          filterEl.datepicker('hide');
          //for autosubmit form
          filterEl.trigger('change');
        }
      }
    });

    $("button.ui-datepicker-reset").live('click', function(){
      var currentFormId = $(this).attr('data-form-id');

      if (currentFormId) {
        if (form_id == currentFormId) {
          filterEl.val("");
          filterEl.datepicker('hide');
          //for autosubmit form
          filterEl.trigger('change');

        }
      }
    });

    var options = initDatepicker(localContext, form_id, filter);
    filterEl.datepicker(options);
  };

  function initDatepicker(localContext, form_id, filter) {
    var filterEl     = $(filter.id, localContext);
    var cur = -1, prv = -1;
    var d1Clicks = 0;
    var dateFormat = 'dd.mm.yy';

    var options = {
      //showOn: "button",
      buttonText: '...',
      dateFormat: dateFormat,
      numberOfMonths: filter.numberOfMonths,
      showButtonPanel: true,

      beforeShow: function(date, inst){
        if (filter.calendarAlignment == 'right') {
          var dpId = $.datepicker._mainDivId;
          setTimeout(function(){
            var l = $(filter.id, localContext).offset().left;
            var w = $("#" + dpId).width();
            var left = (l - w + filterEl.parent().width() - 2);
            left = (left<0)? l:left;
            $("#" + dpId).css({
              "left" : left + "px"
            });
          }, 50);
        }
      },

      beforeShowDay: function ( date ) {
        return [true, ( (date.getTime() >= Math.min(prv, cur) && date.getTime() <= Math.max(prv, cur)) ? 'date-range-selected' : '')];
      },

      onSelect: function ( dateText, inst ) {
        var d1, d2;
        inst.inline = true;
        inst.singleValue = false;

        prv = cur;
        cur = (new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay)).getTime();
        if ( prv == -1 || prv == cur ) {
          prv = cur;
          inst.d1 = dateText;
          d1Clicks++;
        } else {
          d1 = $.datepicker.formatDate( dateFormat, new Date(Math.min(prv, cur)), {} );
          d2 = $.datepicker.formatDate( dateFormat, new Date(Math.max(prv, cur)), {} );
          inst.d1 = d1;
          inst.d2 = d2;
        }

        filterEl.val("");
        if (prv == cur) {
          inst.singleValue = true;
        }
      },

      onAfterUpdate: function(arg1, inst){

        if ($("button.ui-datepicker-select").length) {
          if ($($("button.ui-datepicker-select")).attr("data-form-id")) {
            form_id = $($("button.ui-datepicker-select")).attr("data-form-id");
          }
        }

        var d1 = '', d2 = '';
        if (inst.d1 !== undefined) {
          d1 = inst.d1;
        }

        if (inst.d2 !== undefined) {
          d2 = inst.d2;
        }

        if (inst.singleValue) {
          d2 = d1;
        }

        if ((d1 == d2 || d2 == "") && (d1Clicks % 2 == 0)) {
          d1Clicks = 0;
          d1 = "";
          d2 = "";
          $("td.date-range-selected").removeClass("date-range-selected");
          cur = -1;
          prv = -1;
        }

        filterEl.attr('data-start', d1);
        filterEl.attr('data-end', d2);

        $("div.ui-datepicker-header").each(function(index, data){
          if (!$(data).parents(".ui-datepicker-group").find("td.date-range-selected").length) {
            $(data).find("div.ui-datepicker-title").addClass('inactive-month');
          }
          else {
            $(data).find("div.ui-datepicker-title").removeClass('inactive-month');
          }
        });

        if ($("div.ui-datepicker-buttonpane").find("button").not('[data-handler="today"]').length == 0) {
          $('<input class="ui-datepicker-start" value="' + d1 + '" /> - <input class="ui-datepicker-end" value="' + d2 + '" />' +
            '<button type="button" data-form-id="' + form_id + '" data-filter-id="' + filter.id + '" class="ui-datepicker-select ui-state-default ui-priority-primary ui-corner-all">'+Drupal.t('Apply')+'</button>'+
            '<button type="button" data-form-id="' + form_id + '" data-filter-id="' + filter.id + '" class="ui-datepicker-reset ui-state-default ui-priority-primary ui-corner-all">'+Drupal.t('Reset')+'</button>')
          .appendTo($('div.ui-datepicker-buttonpane'));
        }
      },

      onClose: function(date, inst){
        inst.inline = false;
      }
    };

    return options;
  }

})(jQuery, Drupal);
